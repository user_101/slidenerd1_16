package com.example.materialtest2;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

// 1 When the user runs the app for the first time you and is not comming back from the rotation you
// want to show the drawer
// 2 If the user comes from the rotation you don't want to open the drawer
public class NavigationDrawerFragment extends Fragment{

    private RecyclerView recyclerView;

    // variable for shared preferences
    private static final String PREF_FILE_NAME="testpref";

    //Key for shared preferences
    public static final String KEY_USER_LEARNED_DRAWER="user_learned_drawer";

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    private VivzAdapter adapter;

    // indicate if user is aware of the drawers existance or not
    private boolean mUserLernedDrawer;

    //indicates if the fragment is beeing started for the very frist time or is started from a rotation
    private boolean mFromSavedInstanceState;

    private View containerView;
    private boolean isDrawerOpened = false;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // If drawer is already opened
        mUserLernedDrawer = Boolean.valueOf(readFromPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, "false"));

        //application runs for the first time or just coming back from rotation
        if(savedInstanceState == null){
            mFromSavedInstanceState = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        recyclerView= (RecyclerView) layout.findViewById(R.id.drawerList);
        adapter = new VivzAdapter(getActivity(),getData());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Inflate the layout for this fragment
        return layout;
    }


    public static List<Information> getData(){

        List<Information> data = new ArrayList<>();
        int[] icons = {R.drawable.ic_one_70,R.drawable.ic_two_70,R.drawable.ic_three_70,R.drawable.ic_four_70};
        String[] titles = {"Vivz", "Anky", "Slidenerd", "Youtube"};

        for (int i = 0; i < titles.length && i<icons.length;i++)
        {
            Information current = new Information();
            current.iconId = icons[i];
            current.title = titles[i];
            data.add(current);
        }

        return data;
    }

    public void setUp(int fragmentID, DrawerLayout drawerLayout, final Toolbar toolbar) {

        containerView = getActivity().findViewById(fragmentID);

        mDrawerLayout = drawerLayout ;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close){

            // Gets called when the drawer is completely opened
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                //If the user has never seen the drawer before and is not comming from rotation
                if(!mUserLernedDrawer){
                    mUserLernedDrawer = true;
                    saveToPreferences(getActivity(),KEY_USER_LEARNED_DRAWER, mUserLernedDrawer+"");
                }
                //Redraw the menu
                getActivity().invalidateOptionsMenu();
            }

            // Gets called when the drawer is completely closed
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            //Called when a drawers position changes
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if(slideOffset < 0.6) {
                    toolbar.setAlpha(1 - slideOffset);
                }
            }
        };
        //if the user has never seen the drawer
        if (!mUserLernedDrawer && !mFromSavedInstanceState){
            mDrawerLayout.openDrawer(containerView);
        }

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // To display the hamburger icon
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    // write to shared preference
    public static void saveToPreferences(Context context, String preferenceName, String preferenceValue){
        //mode private = only our app can modify sharedprefrence's value  value
        SharedPreferences sharedPreferences=context.getSharedPreferences(PREF_FILE_NAME, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        //When calling commit you must wait for result; it works synchronously it is slower then apply
        editor.apply();

    }

    //read from shared preferences
    public static String readFromPreferences(Context context, String preferenceName, String defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);

        return sharedPreferences.getString(preferenceName,defaultValue);

    }
}
